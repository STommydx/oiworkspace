#include <iostream>
#include <vector>

// Automod Integer Template {{{
class mint {
private:
	int x;
	static constexpr int MOD = 1'000'000'007;

public:
	friend inline std::ostream &operator<<(std::ostream &os, const mint &arg) {
		return os << arg.x;
	}
	friend inline std::istream &operator>>(std::istream &is, mint &arg) {
		is >> arg.x;
		if (arg.x >= MOD) arg.x -= MOD;
		return is;
	}
	mint(const int &x) : x(x) {
		if (this->x >= MOD) this->x -= MOD;
		if (this->x < 0) this->x += MOD;
	}
	mint(const long long &x) : x(x % MOD) {}
	mint() : x(0) {}

	mint &operator+=(const mint &rhs) {
		x += rhs.x;
		if (x >= MOD) x -= MOD;
		return *this;
	}
	mint &operator++() { return *this += 1; }
	mint operator+(const mint &rhs) const { return mint(*this) += rhs; }
	mint operator++(int) {
		mint cpy(*this);
		++*this;
		return cpy;
	}

	mint &operator-=(const mint &rhs) {
		x -= rhs.x;
		if (x < 0) x += MOD;
		return *this;
	}
	mint &operator--() { return *this -= 1; }
	mint operator-(const mint &rhs) const { return mint(*this) -= rhs; }
	mint operator-() const { return mint() - *this; }
	mint operator--(int) {
		mint cpy(*this);
		--*this;
		return cpy;
	}

	mint &operator*=(const mint &rhs) {
		x = 1LL * x * rhs.x % MOD;
		return *this;
	}
	mint operator*(const mint &rhs) const { return mint(*this) *= rhs; }

	mint pow(long long p) const {
		mint rt = 1, b = *this;
		for (;p;p>>=1,b*=b) if (p & 1) rt *= b;
		return rt;
	}

	mint operator^(long long p) const { return pow(p); }
	mint &operator^=(long long p) { return *this = pow(p); }

	mint &operator/=(const mint &rhs) { return *this *= rhs.pow(MOD - 2); }
	mint operator/(const mint &rhs) const { return mint(*this) /= rhs; }

	constexpr bool operator<(const mint &rhs) const { return x < rhs.x; }
	constexpr bool operator>(const mint &rhs) const { return x > rhs.x; }
	constexpr bool operator<=(const mint &rhs) const { return x <= rhs.x; }
	constexpr bool operator>=(const mint &rhs) const { return x >= rhs.x; }
	constexpr bool operator==(const mint &rhs) const { return x == rhs.x; }
	constexpr bool operator!=(const mint &rhs) const { return x != rhs.x; }

	constexpr explicit operator int() const { return x; }

};
constexpr int mint::MOD;
using vm = std::vector<mint>;
using vvm = std::vector<vm>;
using pmm = std::pair<mint, mint>;
// }}}

// vim: fdm=marker
