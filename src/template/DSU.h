#include <vector>
#include <numeric>

// DSU Template {{{
class DSU {
	int n;
	std::vector<int> p;
public:
	DSU(int sz) : n(sz), p(sz) {
		std::iota(p.begin(), p.end(), 0);
	}
	int &fi(int x) { return p[x] = p[x] - x ? fi(p[x]) : x; }
	inline int operator[](int x) { return fi(x); }
	inline int &un(int u, int v) { return fi(fi(u)) = fi(v); }
};
// }}}

// vim: fdm=marker
