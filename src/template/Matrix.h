#include <vector>
#include <iostream>

// Matrix Template {{{
template<class T>
class Mat {
private:
	int n, m;
	std::vector<std::vector<T>> a;

public:
	Mat(int n, int m) : n(n), m(m), a(n, std::vector<T>(m)) {}
	Mat(int n) : Mat(n, n) {}
	Mat(const std::vector<std::vector<T>> &v) : n(v.size()), m(v[0].size()), a(v) {}

	static Mat<T> I(int n) {
		Mat<T> res(n);
		for (int i=0;i<n;i++) res[i][i] = 1;
		return res;
	}

	std::vector<T> &operator[](int r) {
		return a[r];
	}

	const std::vector<T> &operator[](int r) const {
		return a[r];
	}

	Mat<T> operator*(const Mat<T> &rhs) const {
		Mat<T> res(n, rhs.m);
		for (int i=0;i<n;i++) for (int j=0;j<res.m;j++) for (int k=0;k<m;k++) res[i][j] += a[i][k] * rhs.a[k][j];
		return res;
	}

	Mat<T> operator+(const Mat<T> &rhs) const {
		Mat<T> res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = a[i][j] + rhs.a[i][j];
		return res;
	}

	Mat<T> operator-(const Mat<T> &rhs) const {
		Mat<T> res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = a[i][j] - rhs.a[i][j];
		return res;
	}

	Mat<T> operator-() const {
		Mat<T> res(n, m);
		for (int i=0;i<n;i++) for (int j=0;j<m;j++) res[i][j] = -a[i][j];
		return res;
	}

	Mat<T> operator^(long long x) const {
		Mat<T> bas(*this);
		Mat<T> res(n);
		for (int i=0;i<n;i++) res[i][i] = 1;
		for (;x;x>>=1,bas=bas*bas) if (x & 1) res = res * bas;
		return res;
	}

	Mat<T> submatrix(int x, int y, int xx, int yy) const {
		Mat<T> res(xx - x + 1, yy - y + 1);
		for (int i=x;i<=xx;i++) for (int j=y;j<=yy;j++) res[i-x][j-y] = a[i][j];
		return res;
	}

	Mat<T> concath(const Mat<T> &rhs) const {
		Mat<T> res(n, m + rhs.m);
		for (int i=0;i<n;i++) {
			for (int j=0;j<m;j++) res[i][j] = a[i][j];
			for (int j=0;j<rhs.m;j++) res[i][m+j] = rhs.a[i][j];
		}
		return res;
	}

	void gauselim() {

		// row r as pivot
		for (int r=0;r<n;r++) {
			// find mxrow
			T mx = 0; int mxr = -1;
			for (int i=r;i<n;i++) if (a[i][r] > mx) mx = a[i][r], mxr = i;
			if (mxr == -1) {
				std::cerr << "WTF" << std::endl;
				exit(1);
			}
			for (int j=0;j<m;j++) swap(a[r][j], a[mxr][j]);

			// make first term 1
			T ft = a[r][r];
			for (int j=r;j<m;j++) a[r][j] = a[r][j] / ft;

			// make a[i][r] = 0 for all i > r
			for (int i=r+1;i<n;i++) {
				T needsub = a[i][r];
				for (int j=r;j<m;j++) a[i][j] -= needsub * a[r][j];
			}
		}
		// backward sub
		for (int r=n-1;r>=0;r--) {
			for (int j=r+1;j<n;j++) {
				T needsub = a[r][j];
				for (int k=0;k<m;k++) a[r][k] -= needsub * a[j][k];
			}
		}
	}

	Mat<T> inverse() const {
		Mat<T> gus = concath(Mat::I(n));
		gus.gauselim();
		return gus.submatrix(0, m, n-1, m+m-1);
	}

	friend std::ostream &operator<<(std::ostream &os, const Mat<T> &mt) {
		for (int i=0;i<mt.n;i++) for (int j=0;j<mt.m;j++) os << mt.a[i][j] << " \n"[j==mt.m-1];
		return os;
	}

	friend std::istream &operator>>(std::istream &is, Mat<T> &mt) {
		for (int i=0;i<mt.n;i++) for (int j=0;j<mt.m;j++) is >> mt.a[i][j];
		return is;
	}

};
// }}}

// vim: fdm=marker
