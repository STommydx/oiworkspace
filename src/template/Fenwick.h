#include <vector>

// Fenwick Tree {{{
template <class T>
class Fenwick {
	private:
		std::vector<T> arr;
		int sz;
		T myQuery(int idx) {
			T rt{};
			for(;idx>0;idx-=idx&-idx) rt += arr[idx];
			return rt;
		}
		void myUpdate(int idx, const T &val) {
			for(;idx<=sz;idx+=idx&-idx) arr[idx] += val;
		}
	public:
		Fenwick(int n) : arr(n + 1), sz(n) {}
		T query(int x) { return myQuery(x + 1); }
		T query(int l, int r) { return query(r) - query(l - 1); }
		void update(int x, const T &v) { myUpdate(x + 1, v); }
};
// }}}

// Range Update Fenwick {{{
template <class T>
class RangeFenwick {
	private:
		Fenwick<T> tr1, tr2;
	public:
		RangeFenwick(int n) : tr1(n), tr2(n) {}
		T query(int x) { return (x + 1) * tr1.query(x) - tr2.query(x); }
		T query(int l, int r) { return query(r) - query(l - 1); }
		void update(int l, int r, const T &v) {
			tr1.update(l, v);
			tr1.update(r + 1, -v);
			tr2.update(l, v * l);
			tr2.update(r + 1, -v * (r + 1));
		}
};
// }}}

// vim: fdm=marker
