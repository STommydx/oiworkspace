#include <vector>
#include <limits>
template <class T> struct Inf { static constexpr T inf() { return std::numeric_limits<T>::has_infinity() ? std::numeric_limits<T>::infinty() : std::numeric_limits<T>::max(); } };

// Classic Segment Tree {{{
template <class T, class U, class SegOp>
class SegmentTree {
private:
	SegOp op;
	int n;
	std::vector<T> tree;

public:
	SegmentTree(const std::vector<T> &init) : n(init.size()), tree(init.size()) {
		for (const T &x : init) tree.push_back(x);
		for (int i = n - 1; i > 0; i--) tree[i] = op(tree[i << 1], tree[i << 1 | 1]);
	}
	SegmentTree(int n, const T &init) : SegmentTree(std::vector<T>(n, init)) {}
	SegmentTree(int n) : SegmentTree(n, op.def()) {}

	void modify(int p, const U &val) {
		for (op.upd(tree[p += n], val); p >>= 1; ) tree[p] = op(tree[p << 1], tree[p << 1 | 1]);
	}

	T query(int l, int r) {
		T resl = op.def(), resr = op.def();
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) resl = op(resl, tree[l++]);
			if (r & 1) resr = op(tree[--r], resr);
		}
		return op(resl, resr);
	}

};
// }}}

// Range Update Segment Tree {{{
template <class T, class U, class SegOp>
class RangeSegmentTree {
private:
	SegOp op;
	int n;
	std::vector<T> tree;

public:
	RangeSegmentTree(const std::vector<T> &init) : n(init.size()), tree(init.size()) {
		for (const T &x : init) tree.push_back(x);
		for (int i = n - 1; i > 0; i--) tree[i] = op(tree[i << 1], tree[i << 1 | 1]);
	}
	RangeSegmentTree(int n, const T &init) : RangeSegmentTree(std::vector<T>(n, init)) {}
	RangeSegmentTree(int n) : RangeSegmentTree(n, op.def()) {}

	T query(int p) {
		T res = op.def();
		for (p += n; p > 0; p >>= 1) res = op(res, tree[p]);
		return res;
	}

	void modify(int l, int r, const U &val) {
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) op.upd(tree[l++], val);
			if (r & 1) op.upd(tree[--r], val);
		}
	}
};
// }}}

// Lazy Segment Tree {{{
template <class T, class U, class LazyOp>
class LazySegmentTree {
private:
	LazyOp op;
	int n, h;
	std::vector<T> tree;
	std::vector<U> lazy;
	std::vector<bool> tag;

	void calc(int p, int len) {
		tree[p] = op(tree[p << 1], tree[p << 1 | 1]);
		if (tag[p]) op.upd(tree[p], lazy[p], len);
	}

	void apply(int p, const U &val, int len) {
		op.upd(tree[p], val, len);
		if (p < n) op.upLazy(lazy[p], val), tag[p] = true;
	}

	void build(int p) {
		int len = 2;
		for (p += n; p >>= 1; len <<= 1) calc(p, len);
	}

	void push(int p) {
		int s = h, len = 1 << (h - 1);
		for (p += n; s > 0; s--, len >>= 1) {
			int i = p >> s;
			if (tag[i]) {
				apply(i << 1, lazy[i], len);
				apply(i << 1 | 1, lazy[i], len);
				lazy[i] = op.udef(), tag[i] = false;
			}
		}
	}

public:
	LazySegmentTree(const std::vector<T> &init) : n(init.size()), h(0), tree(n), lazy(n + n, op.udef()), tag(n + n) {
		for (const T &x : init) tree.push_back(x);
		while ((1 << h) <= n) h++;
		for (int i = n - 1; i > 0; i--) tree[i] = op(tree[i << 1], tree[i << 1 | 1]);
	}
	LazySegmentTree(int n, const T &init) : LazySegmentTree(std::vector<T>(n, init)) {}
	LazySegmentTree(int n) : LazySegmentTree(n, op.def()) {}

	void modify(int l, int r, const U &val) {
		push(l); push(r);
		int l0 = l, r0 = r, len = 1;
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1, len <<= 1) {
			if (l & 1) apply(l++, val, len);
			if (r & 1) apply(--r, val, len);
		}
		build(l0); build(r0);
	}

	T query(int l, int r) {
		push(l); push(r);
		T res = op.def();
		for (l += n, r += n + 1; l < r; l >>= 1, r >>= 1) {
			if (l & 1) res = op(res, tree[l++]);
			if (r & 1) res = op(tree[--r], res);
		}
		return res;
	}

};
// }}}

// Dynamic Segment Tree {{{
template <class T, class U, class SegOp, class SegIndex = int>
class DynamicSegmentTree {
private:

	struct SegNode;
	using SegPointer = SegNode *;
	// using SegPointer = unique_ptr<SegNode>;

	struct SegNode {
		T tree;
		U lazy;
		SegPointer lchi, rchi;
	};

	SegOp op;
	SegIndex n;
	SegPointer root = nullptr;
	const T defVal;

	void refreshNode(SegPointer &u, SegIndex lo, SegIndex hi) {
		if (u) return;
		u = SegPointer{new SegNode{defVal, op.udef(), nullptr, nullptr}};
	}

	void apply(const U &val, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		op.upd(u->tree, val, hi - lo + 1);
		op.upLazy(u->lazy, val);
	}

	void push(SegPointer &u, SegIndex lo, SegIndex hi) {
		if (u->lazy != op.udef()) {
			SegIndex mi = lo + (hi - lo) / 2;
			apply(u->lazy, u->lchi, lo, mi);
			apply(u->lazy, u->rchi, mi + 1, hi);
			u->lazy = op.udef();
		}
	}

	void build(SegPointer &u, SegIndex lo, SegIndex hi, const std::vector<T> &init) {
		refreshNode(u, lo, hi);
		if (lo == hi) {
			u->tree = init[lo];
		} else {
			SegIndex mi = lo + (hi - lo) / 2;
			build(u->lchi, lo, mi, init);
			build(u->rchi, mi + 1, hi, init);
			u->tree = op(u->lchi->tree, u->rchi->tree);
		}
	}

	void modify(SegIndex l, SegIndex r, const U &val, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		if (r < lo || l > hi || l > r) return;
		if (l <= lo && hi <= r) {
			apply(val, u, lo, hi);
			return;
		}
		push(u, lo, hi);
		SegIndex mi = lo + (hi - lo) / 2;
		modify(l, r, val, u->lchi, lo, mi);
		modify(l, r, val, u->rchi, mi + 1, hi);
		u->tree = op(u->lchi->tree, u->rchi->tree);
	}

	T query(SegIndex l, SegIndex r, SegPointer &u, SegIndex lo, SegIndex hi) {
		refreshNode(u, lo, hi);
		if (r < lo || l > hi || l > r) return op.def();
		if (l <= lo && hi <= r) {
			return u->tree;
		}
		push(u, lo, hi);
		SegIndex mi = lo + (hi - lo) / 2;
		T &&lres = query(l, r, u->lchi, lo, mi);
		T &&rres = query(l, r, u->rchi, mi + 1, hi);
		return op(lres, rres);
	}

public:
	DynamicSegmentTree(const std::vector<T> &init) : n(init.size()) {
		build(root, 0, n - 1, init);
	}
	DynamicSegmentTree(SegIndex n, const T &def) : n(n), defVal(def) {}
	DynamicSegmentTree(SegIndex n) : n(n), defVal(op.def()) {}

	void modify(SegIndex l, SegIndex r, const U &val) {
		modify(l, r, val, root, 0, n - 1);
	}

	T query(SegIndex l, SegIndex r) {
		return query(l, r, root, 0, n - 1);
	}

};
// }}}

// Segment Tree Operators {{{
template <class T, class U> struct SegOpTemp {
	constexpr T operator()(const T &lhs, const T &rhs); // query (e.g. range max/sum)
	constexpr T &upd(T &lhs, const U &rhs, int len = 1); // update (e.g. range add/assign)
	constexpr U &upLazy(U &lhs, const U &rhs); // update for lazy nodes (e.g. sum update accumulate)
	constexpr const T def(); // default value for result (e.g. INF for min query)
	constexpr const U udef(); // default value for update (e.g. 0 for add updates)
};

template <class T, class U> struct SumAdd {
	constexpr T operator()(const T &lhs, const T &rhs) { return lhs + rhs; }
	constexpr T &upd(T &lhs, const U &rhs, int len = 1) { return lhs += rhs * len; }
	constexpr U &upLazy(U &lhs, const U &rhs) { return lhs += rhs; }
	constexpr const T def() { return 0; }
	constexpr const U udef() { return 0; }
};

template <class T, class U> struct SumAsg {
	constexpr T operator()(const T &lhs, const T &rhs) { return lhs + rhs; }
	constexpr T &upd(T &lhs, const U &rhs, int len = 1) { return lhs = rhs * len; }
	constexpr U &upLazy(U &lhs, const U &rhs) { return lhs = rhs; }
	constexpr const T def() { return 0; }
	constexpr const U udef() { return 0; }
};

template <class T, class U> struct MinAdd {
	constexpr T operator()(const T &lhs, const T &rhs) { return min(lhs, rhs); }
	constexpr T &upd(T &lhs, const U &rhs, int = 1) { return lhs += rhs; }
	constexpr U &upLazy(U &lhs, const U &rhs) { return lhs += rhs; }
	constexpr const T def() { return Inf<T>::inf(); }
	constexpr const U udef() { return 0; }
};

template <class T, class U> struct MinAsg {
	constexpr T operator()(const T &lhs, const T &rhs) { return min(lhs, rhs); }
	constexpr T &upd(T &lhs, const U &rhs, int = 1) { return lhs = rhs; }
	constexpr U &upLazy(U &lhs, const U &rhs) { return lhs = rhs; }
	constexpr const T def() { return Inf<T>::inf(); }
	constexpr const U udef() { return 0; }
};
// }}}

// vim: fdm=marker
