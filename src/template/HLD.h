#include "NewSegTree.h"
#include <functional>

template <class T, class SegOp>
class HeavyLight {
private:
	int n;
	std::vector<int> par, heavy, dep;
	std::vector<int> root, treePos;
	SegmentTree<T, SegOp> st;
	SegOp myop;

	template <class O>
	void process(int u, int v, O op) {
		// lift to same chain
		for (; root[u] != root[v]; v = par[root[v]]) {
			// process deeper node first
			if (dep[root[u]] > dep[root[v]]) std::swap(u, v);
			op(treePos[root[v]], treePos[v]);
		}
		if (dep[u] > dep[v]) std::swap(u, v);
		// query that chain
		// op(treePos[u], treePos[v]); // vertex query
		op(treePos[u] + 1, treePos[v]); // edge query
	}

public:
	HeavyLight(const std::vector<std::vector<int>> &g) :
	n(g.size()),
	par(n), heavy(n, -1), dep(n),
	root(n), treePos(n),
	st(std::vector<T>(n, myop.def())) {
		par[0] = -1;
		const std::function<int(int)> dfs = [&](int u) {
			int sz = 1, mxz = 0;
			for (int v : g[u]) if (v != par[u]) {
				par[v] = u;
				dep[v] = dep[u] + 1;
				int subsz = dfs(v);
				if (subsz > mxz) heavy[u] = v, mxz = subsz;
				sz += subsz;
			}
			return sz;
		};
		dfs(0);
		for (int i=0, now=0; i<n; i++) {
			// test if head
			if (par[i] == -1 || heavy[par[i]] != i) {
				// iterate through the chain
				for (int j=i;j!=-1;j=heavy[j]) {
					root[j] = i; // set first node for chain
					treePos[j] = now++; // set position in seg tree
				}
			}
		}
	}

	void modify(int u, int v, const T &val) {
		process(u, v, [this, &val](int l, int r) {st.modify(l, r, val);});
	}

	void modify(int u, const T &val) {
		st.modify(treePos[u], treePos[u], val);
	}

	T query(int u, int v) {
		T rt = myop.def();
		process(u, v, [this, &rt](int l, int r) {rt = myop(rt, st.query(l, r));});
		return rt;
	}

	int getParent(int u) {
		return par[u];
	}

};
