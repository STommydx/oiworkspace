#include <functional>
using namespace std;

template <class T, class Operation = std::plus<T>>
class LazySegmentTree {
private:
	int n;
	std::vector<T> tree, lazy;
	Operation op;

	void apply(const T &val, int u, int lo, int hi) {
		// tree[u] += val * (hi - lo + 1);
		// lazy[u] += val;
		tree[u] = val;
		lazy[u] = op(lazy[u], val);
	}

	void push(int u, int lo, int hi) {
		if (lazy[u] != T()) {
			int mi = (lo + hi) >> 1;
			apply(lazy[u], u << 1, lo, mi);
			apply(lazy[u], u << 1 | 1, mi + 1, hi);
			lazy[u] = T();
		}
	}

	void build(int u, int lo, int hi, const std::vector<T> &init) {
		if (lo == hi) {
			tree[u] = init[lo];
		} else {
			int mi = (lo + hi) >> 1;
			build(u << 1, lo, mi, init);
			build(u << 1 | 1, mi + 1, hi, init);
			tree[u] = op(tree[u << 1], tree[u << 1 | 1]);
		}
	}

	void modify(int l, int r, const T &val, int u, int lo, int hi) {
		if (r < lo || l > hi || l > r) return;
		if (l <= lo && hi <= r) {
			apply(val, u, lo, hi);
			return;
		}
		push(u, lo, hi);
		int mi = (lo + hi) >> 1;
		modify(l, r, val, u << 1, lo, mi);
		modify(l, r, val, u << 1 | 1, mi + 1, hi);
		tree[u] = op(tree[u << 1], tree[u << 1 | 1]);
	}

	T query(int l, int r, int u, int lo, int hi) {
		if (r < lo || l > hi || l > r) return T();
		if (l <= lo && hi <= r) {
			return tree[u];
		}
		push(u, lo, hi);
		int mi = (lo + hi) >> 1;
		T &&lres = query(l, r, u << 1, lo, mi);
		T &&rres = query(l, r, u << 1 | 1, mi + 1, hi);
		return op(lres, rres);
	}

public:
	LazySegmentTree(const std::vector<T> &init) :
	n(init.size()),
	tree(n * 4),
	lazy(n * 4) {
		build(1, 0, n - 1, init);
	}

	void modify(int l, int r, const T &val) {
		modify(l, r, val, 1, 0, n - 1);
	}

	T query(int l, int r) {
		return query(l, r, 1, 0, n - 1);
	}

};
